# Easy Entity Base Field

Are you boring with too many field tables in your database?

When hundreds of data fields are added to the content node, and multi-version
is enabled for the content node, the total number of field data tables and
field multi-version tables created in the database would be double or even
mort. In fact, it's ok to add some fields to the entity table.

The Easy entity base field module can add basic fields to the data table
data_table of all content entities, such as: Node, User, block_content, etc.
The added fields can be managed using form display and display which are
build-in drupal core by default.

You can also add field types without UI in the core online, such as: Map, URI,
File URI, Password, but they can only be storaged as data, and the form needs
to be customized in the entity addition form.

Multi valued fields are not supported. Please use the core module Field for
multi valued fields,Due to the problem that some field type setting
(fieldSettingsForm) forms and storage (storageSettingsForm) forms cannot be
used in Easy entity base field module, but could still reorganize the
corresponding forms with modules such as: entity_reference and
dynamic_entity_reference in module plug-ins.

If you are worried that too much field data in a table will affect performance,
you need programmers to cooperate with the Field module in the core according
to project requirements, or create custom fields.

**Instructions:**
1. Install the module normally;
1. Visit admin/config/content/easy-entity-field to enable the content type;
1. Access the Easy entity base field field management interface that enables
  the content type;
1. Exportable field configuration file:
  easy_entity_field.field.entity_type_id.field_name.yml, placed in a custom
  module, creates fields synchronously during installation.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/easy_entity_field).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/easy_entity_field).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module doesn't require any module outside of Drupal core.

**Required patches for the module**
If MAP and LINK fields are created, a warning will be displayed.

Deprecated function: unserialize(): Passing null to parameter #1 ($data) of
type string is deprecated in Drupal\Core\Entity\Sql\SqlContentEntityStorage->
loadFromSharedTables() (line 602

Please use the patch:
[https://www.drupal.org/files/issues/2023-09-29/3300404-48-map-field-failing-php-81.patch](https://www.drupal.org/files/issues/2023-09-29/3300404-48-map-field-failing-php-81.patch)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Visit admin/config/content/easy-entity-field to enable the content type;
1. Access the Easy entity base field field management interface that enables the
   content type;
1. Exportable field configuration file:
   easy_entity_field.field.entity_type_id.field_name.yml, placed in 4. custom
   module, creates fields synchronously during installation.


## Maintainers

- 翔 高  - [qiutuo](https://www.drupal.org/u/qiutuo)