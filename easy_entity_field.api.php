<?php

/**
 * @file
 * Hooks specific to the Easy entity field module.
 */

/**
 * Remove the specified field type from the list of added new fields.
 *
 * @param array $field_types
 *   The field type to remove.
 *
 * @return array
 *   Remove field type.
 */
function hook_easy_entity_field_remove_field_type(array $field_types): array {
  $field_types['field_type_name'] = TRUE;
  return $field_types;
}

/**
 * Remove the specified entity type from the settings list.
 *
 * @param array $entity_types
 *   The entity type to remove.
 *
 * @return array
 *   Remove entity type.
 */
function hook_easy_entity_field_remove_entity_type(array $entity_types): array {
  $entity_types['entity_type_id'] = TRUE;
  return $entity_types;
}
