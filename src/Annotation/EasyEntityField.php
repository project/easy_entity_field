<?php

namespace Drupal\easy_entity_field\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a Easy entity field item annotation object.
 *
 * @see \Drupal\easy_entity_field\Plugin\EasyEntityFieldManager
 * @see plugin_api
 *
 * @Annotation
 */
class EasyEntityField extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The easy entity field label.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The component description.
   *
   * @var string
   */
  public string $description;

}
