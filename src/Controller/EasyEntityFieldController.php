<?php

namespace Drupal\easy_entity_field\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Easy entity base field routes.
 */
class EasyEntityFieldController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(): array {
    return $this->entityTypeManager()->getListBuilder('easy_entity_field')->render(TRUE);
  }

}
