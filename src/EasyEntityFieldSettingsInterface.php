<?php

namespace Drupal\easy_entity_field;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Gets the content entity type with the Easy Entity field enabled.
 */
interface EasyEntityFieldSettingsInterface extends ContainerInjectionInterface {

  /**
   * Get the data for enabling the Easy Entity field type setting.
   *
   * @return array
   *   Enable the list of entity types in the Easy Entity field.
   */
  public function getEntityTypes(): array;

}
