<?php

namespace Drupal\easy_entity_field;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface;
use Drupal\Core\Entity\EntityTypeListenerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionListenerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The entity definition update manager.
 */
final class EasyEntityUpdate implements ContainerInjectionInterface {

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  private EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager;

  /**
   * The last installed schema repository.
   *
   * @var \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface
   */
  private EntityLastInstalledSchemaRepositoryInterface $entityLastInstalledSchemaRepository;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type listener service.
   *
   * @var \Drupal\Core\Entity\EntityTypeListenerInterface
   */
  private EntityTypeListenerInterface $entityTypeListener;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private EntityFieldManagerInterface $entityFieldManager;

  /**
   * The field storage definition listener service.
   *
   * @var \Drupal\Core\Field\FieldStorageDefinitionListenerInterface
   */
  private FieldStorageDefinitionListenerInterface $fieldStorageDefinitionListener;

  /**
   * Constructs a new EntityDefinitionUpdateManager.
   *
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $entity_definition_update_manager
   *   The entity definition update manager.
   * @param \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface $entity_last_installed_schema_repository
   *   The last installed schema repository service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeListenerInterface $entity_type_listener
   *   The entity type listener interface.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Field\FieldStorageDefinitionListenerInterface $field_storage_definition_listener
   *   The field storage definition listener service.
   */
  public function __construct(
    EntityDefinitionUpdateManagerInterface $entity_definition_update_manager,
    EntityLastInstalledSchemaRepositoryInterface $entity_last_installed_schema_repository,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeListenerInterface $entity_type_listener,
    EntityFieldManagerInterface $entity_field_manager,
    FieldStorageDefinitionListenerInterface $field_storage_definition_listener
  ) {
    $this->entityDefinitionUpdateManager = $entity_definition_update_manager;
    $this->entityLastInstalledSchemaRepository = $entity_last_installed_schema_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeListener = $entity_type_listener;
    $this->entityFieldManager = $entity_field_manager;
    $this->fieldStorageDefinitionListener = $field_storage_definition_listener;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.definition_update_manager'),
      $container->get('entity.last_installed_schema.repository'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.listener'),
      $container->get('entity_field.manager'),
      $container->get('field_storage_definition.listener')
    );
  }

  /**
   * Clears all cached menu data.
   *
   * @return void
   *   Flushes plugins caches.
   */
  public static function flushCached(): void {
    \Drupal::entityTypeManager()->clearCachedDefinitions();
    \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();
    \Drupal::service("router.builder")->rebuild();
    \Drupal::service('plugin.manager.menu.contextual_link')->clearCachedDefinitions();
    \Drupal::service('plugin.manager.menu.local_task')->clearCachedDefinitions();
    \Drupal::service('plugin.manager.menu.local_action')->clearCachedDefinitions();
  }

  /**
   * Applies all the detected valid changes.
   *
   * Use this with care, as it will apply updates for any module, which will
   * lead to unpredictable results.
   *
   * @param string|null $entity_type_id
   *   The entity type ID for this field.
   */
  public function applyEntityUpdates(string $entity_type_id = NULL): void {
    // Ensure this works also on Drupal 8.6 and earlier.
    $reflector = new \ReflectionMethod($this->entityDefinitionUpdateManager, 'getChangeList');
    $complete_change_list = $reflector->invoke($this->entityDefinitionUpdateManager);

    if (!empty($entity_type_id)) {
      $complete_change_list = array_intersect_key($complete_change_list, [
        $entity_type_id => TRUE,
      ]);
    }

    if ($complete_change_list) {
      // In case there are changes, explicitly invalidate caches.
      $this->entityTypeManager->clearCachedDefinitions();
      $this->entityFieldManager->clearCachedFieldDefinitions();
    }

    foreach ($complete_change_list as $entity_type_id => $change_list) {
      if (!empty($change_list['entity_type'])) {
        $this->doEntityFieldUpdate($change_list['entity_type'], $entity_type_id);
      }
      if (!empty($change_list['field_storage_definitions'])) {
        $storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions($entity_type_id);
        $original_storage_definitions = $this->entityLastInstalledSchemaRepository->getLastInstalledFieldStorageDefinitions(
          $entity_type_id
        );

        foreach ($change_list['field_storage_definitions'] as $field_name => $change) {
          $storage_definition = $storage_definitions[$field_name] ?? NULL;
          $original_storage_definition = $original_storage_definitions[$field_name] ?? NULL;
          $this->doFieldUpdate($change, $storage_definition, $original_storage_definition);
        }
      }
    }
  }

  /**
   * Performs an entity type definition update.
   *
   * @param string $op
   *   The operation to perform, either static::DEFINITION_CREATED or
   *   static::DEFINITION_UPDATED.
   * @param string $entity_type_id
   *   The entity type ID for this field.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function doEntityFieldUpdate(string $op, string $entity_type_id): void {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $field_storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions($entity_type_id);
    if ($op == EntityDefinitionUpdateManagerInterface::DEFINITION_UPDATED) {
      $original = $this->entityLastInstalledSchemaRepository->getLastInstalledDefinition($entity_type_id);
      $original_field_storage_definitions = $this->entityLastInstalledSchemaRepository->getLastInstalledFieldStorageDefinitions(
        $entity_type_id
      );

      $this->entityTypeListener->onFieldableEntityTypeUpdate(
        $entity_type,
        $original,
        $field_storage_definitions,
        $original_field_storage_definitions
      );
    }
  }

  /**
   * Performs a field storage definition update.
   *
   * @param string $op
   *   The operation to perform, possible values are
   *   static::DEFINITION_CREATED,
   *   static::DEFINITION_UPDATED or static::DEFINITION_DELETED.
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface|null $storage_definition
   *   The new field storage definition.
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface|null $original_storage_definition
   *   The original field storage definition.
   */
  public function doFieldUpdate(string $op, FieldStorageDefinitionInterface $storage_definition = NULL, FieldStorageDefinitionInterface $original_storage_definition = NULL): void {
    switch ($op) {
      case EntityDefinitionUpdateManagerInterface::DEFINITION_CREATED:
        $this->fieldStorageDefinitionListener->onFieldStorageDefinitionCreate($storage_definition);
        break;

      case EntityDefinitionUpdateManagerInterface::DEFINITION_UPDATED:
        if ($storage_definition && $original_storage_definition) {
          $this->fieldStorageDefinitionListener->onFieldStorageDefinitionUpdate($storage_definition, $original_storage_definition);
        }
        break;

      case EntityDefinitionUpdateManagerInterface::DEFINITION_DELETED:
        if ($original_storage_definition) {
          $this->fieldStorageDefinitionListener->onFieldStorageDefinitionDelete($original_storage_definition);
        }
        break;
    }
  }

}
