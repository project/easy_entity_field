<?php

namespace Drupal\easy_entity_field\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\FieldableEntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldException;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\easy_entity_field\Plugin\EasyEntityFieldPluginCollection;

/**
 * Defines the Easy entity field entity.
 *
 * @ConfigEntityType(
 *   id = "easy_entity_field",
 *   label = @Translation("Easy entity field"),
 *   handlers = {
 *     "list_builder" = "Drupal\easy_entity_field\EasyEntityFieldListBuilder",
 *     "form" = {
 *       "edit" = "Drupal\easy_entity_field\Form\EasyEntityFieldForm",
 *       "storage" = "Drupal\easy_entity_field\Form\EasyEntityFieldStorageForm",
 *       "delete" = "Drupal\easy_entity_field\Form\EasyEntityFieldDeleteForm"
 *     },
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "field_name",
 *     "entity_type",
 *     "field_type",
 *     "module",
 *     "description",
 *     "field_settings",
 *     "storage_settings",
 *     "required",
 *     "display",
 *     "default_value"
 *   },
 *   config_prefix = "field",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class EasyEntityField extends ConfigEntityBase implements EasyEntityFieldInterface {

  use StringTranslationTrait;

  public const NAME_MAX_LENGTH = 32;

  /**
   * The Easy entity field ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The Easy entity field label.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  protected $label;

  /**
   * The field name.
   *
   * @var string
   */
  protected string $field_name;

  /**
   * The Field type.
   *
   * @var string
   */
  protected string $field_type;

  /**
   * The name of the entity type the field is attached to.
   *
   * @var string
   */
  protected string $entity_type;

  /**
   * The name of the module that provides the field type.
   *
   * @var string
   */
  protected string $module;

  /**
   * The field description.
   *
   * A human-readable description for the field when used with this bundle.
   * For example, the description will be the help text of Form API elements for
   * this field in entity edit forms.
   *
   * @var string
   */
  protected string $description = '';

  /**
   * Field-type specific settings.
   *
   * An array of key/value pairs. The keys and default values are defined by the
   * field type.
   *
   * @var array
   */
  protected array $field_settings = [];

  /**
   * Field-type specific storage settings.
   *
   * An array of key/value pairs, The keys and default values are defined by the
   * field type.
   *
   * @var array
   */
  protected array $storage_settings = [];

  /**
   * Flag indicating whether the field is required.
   *
   * TRUE if a value for this field is required when used with this bundle,
   * FALSE otherwise. Currently, required-ness is only enforced at the Form API
   * level in entity edit forms, not during direct API saves.
   *
   * @var bool
   */
  protected bool $required = FALSE;

  /**
   * Set up form display and view display.
   *
   * @var array
   */
  protected array $display = [];

  /**
   * Default field value.
   *
   * The default value is used when an entity is created, either:
   * - through an entity creation form; the form elements for the field are
   *   prepopulated with the default value.
   * - through direct API calls (i.e. $entity->save()); the default value is
   *   added if the $entity object provides no explicit entry (actual values or
   *   "the field is empty") for the field.
   *
   * The default value is expressed as a numerically indexed array of items,
   * each item being an array of key/value pairs matching the set of 'columns'
   * defined by the "field schema" for the field type, as exposed in the class
   * implementing \Drupal\Core\Field\FieldItemInterface::schema() method. If the
   * number of items exceeds the cardinality of the field, extraneous items will
   * be ignored.
   *
   * @var array
   */
  protected array $default_value = [];

  /**
   * Flag indicating whether the field is deleted.
   *
   * The delete() method marks the field as "deleted" and removes the
   * corresponding entry from the config storage, but keeps its definition in
   * the state storage while field data is purged by a separate
   * garbage-collection process.
   *
   * Deleted fields stay out of the regular entity lifecycle (notably, their
   * values are not populated in loaded entities, and are not saved back).
   *
   * @var bool
   */
  protected bool $deleted = FALSE;

  /**
   * The base field definition.
   *
   * @var \Drupal\Core\Field\BaseFieldDefinition
   */
  protected $baseFieldDefinition;

  /**
   * The plugin collection that holds the plugin for this entity.
   *
   * @var \Drupal\easy_entity_field\Plugin\EasyEntityFieldPluginCollection
   */
  protected EasyEntityFieldPluginCollection $pluginCollection;

  /**
   * Constructs a FieldStorageConfig object.
   *
   * In most cases, Field entities are created via
   * FieldStorageConfig::create($values)), where $values is the same parameter
   * as in this constructor.
   *
   * @param array $values
   *   An array of field properties, keyed by property name. Most array
   *   elements will be used to set the corresponding properties on the class;
   *   see the class property documentation for details. Some array elements
   *   have special meanings and a few are required. Special elements are:
   *   - name: required. As a temporary Backwards Compatibility layer right now,
   *     a 'field_name' property can be accepted in place of 'id'.
   *   - entity_type: required.
   *   - type: required.
   * @param string $entity_type
   *   (optional) The entity type on which the field should be created.
   *   Defaults to "field_storage_config".
   */
  public function __construct(array $values, $entity_type = 'easy_entity_field') {
    if (empty($values['field_name'])) {
      throw new FieldException(
        'Attempt to create a field storage without a field name.'
      );
    }
    if (!preg_match('/^[_a-z]+[_a-z0-9]*$/', $values['field_name'])) {
      throw new FieldException(
        "Attempt to create a field storage {$values['field_name']} with invalid characters. Only lowercase alphanumeric characters and underscores are allowed, and only lowercase letters and underscore are allowed as the first character"
      );
    }
    if (empty($values['field_type'])) {
      throw new FieldException(
        "Attempt to create a field storage {$values['field_name']} with no field type."
      );
    }

    if (empty($values['entity_type'])) {
      throw new FieldException(
        "Attempt to create a field storage {$values['field_name']} with no entity_type."
      );
    }

    parent::__construct($values, $entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function id(): int|string {
    return $this->entity_type . '.' . $this->field_name;
  }

  /**
   * Overrides \Drupal\Core\Entity\Entity::preSave().
   *
   * @throws \Drupal\Core\Field\FieldException|\Exception
   *   If the field definition is invalid.
   *   In case of failures at the configuration storage level.
   */
  public function preSave(EntityStorageInterface $storage): void {
    if ($this->isNew()) {
      $this->preSaveNew();
    }
    else {
      $this->preSaveUpdated();
    }
    parent::preSave($storage);
  }

  /**
   * Prepares saving a new field definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the field type does not exist.
   */
  protected function preSaveNew(): void {
    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager */
    $entity_field_manager = \Drupal::service('entity_field.manager');

    /** @var \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager */
    $field_type_manager = $this->getFieldTypeManager();
    $field_type_definition = $field_type_manager->getDefinition($this->getFieldType(), FALSE);
    // Assign the ID.
    $this->id = $this->id();

    // Field name cannot be longer than FieldStorageConfig::NAME_MAX_LENGTH
    // characters. We use mb_strlen() because the DB layer assumes that column
    // widths are given in characters rather than bytes.
    if (mb_strlen($this->getName()) > static::NAME_MAX_LENGTH) {
      throw new FieldException('Attempt to create a field storage with an name longer than ' . static::NAME_MAX_LENGTH . ' characters: ' . $this->getName());
    }

    // Disallow reserved field names.
    $disallowed_field_names = array_keys($entity_field_manager->getBaseFieldDefinitions($this->getTargetEntityTypeId()));
    if (in_array($this->getName(), $disallowed_field_names)) {
      throw new FieldException("Attempt to create field storage {$this->getName()} which is reserved by entity type {$this->getTargetEntityTypeId()}.");
    }

    // Check that the field type is known.
    if (!$field_type_definition) {
      throw new FieldException("Attempt to create a field storage of unknown type {$this->getFieldType()}.");
    }

    $this->module = $field_type_definition['provider'];
  }

  /**
   * Prepares saving an updated field definition.
   */
  protected function preSaveUpdated(): void {
    $this->field_settings = array_intersect_key($this->field_settings, $this->getFieldSettings()) + $this->getFieldSettings();
    $this->storage_settings = array_intersect_key($this->storage_settings, $this->getStorageSettings()) + $this->getStorageSettings();
    // Some updates are always disallowed.
    if ($this->getFieldType() != $this->original->getFieldType()) {
      throw new FieldException(sprintf('Cannot change the field type for an existing field storage. The field storage %s has the type %s.', $this->id(), $this->original->getFieldType()));
    }
    if ($this->getTargetEntityTypeId() != $this->original->getTargetEntityTypeId()) {
      throw new FieldException(sprintf('Cannot change the entity type for an existing field storage. The field storage %s has the type %s.', $this->id(), $this->original->getTargetEntityTypeId()));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    \Drupal::service('easy_entity_field.entity_update')->applyEntityUpdates($this->getTargetEntityTypeId());
    \Drupal::service('easy_entity_field.entity_update')->flushCached();
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $fields) {
    /** @var \Drupal\Core\Field\DeletedFieldsRepositoryInterface $deleted_fields_repository */
    $deleted_fields_repository = \Drupal::service('entity_field.deleted_fields_repository');
    $entity_type_manager = \Drupal::entityTypeManager();
    parent::preDelete($storage, $fields);

    foreach ($fields as $field) {
      if ($field->isTargetEntityType()) {
        $target_entity_storage = $entity_type_manager->getStorage($field->getTargetEntityTypeId());

        if (!$field->deleted && $target_entity_storage instanceof FieldableEntityStorageInterface && $target_entity_storage->countFieldData($field->getFieldStorageDefinition(), TRUE)) {
          $field = clone $field;
          $field->deleted = TRUE;
          $field->baseFieldDefinition = NULL;
          $deleted_fields_repository->addFieldDefinition($field->getFieldStorageDefinition());
          $deleted_fields_repository->addFieldStorageDefinition($field->getFieldStorageDefinition());
        }
        \Drupal::service('easy_entity_field.entity_update')->applyEntityUpdates($field->getTargetEntityTypeId());
        \Drupal::entityTypeManager()->clearCachedDefinitions();
        \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $fields) {
    \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();

    foreach ($fields as $field) {
      if (!$field->deleted && $field->isTargetEntityType()) {
        $field_storage_definition = \Drupal::service('entity.last_installed_schema.repository')->getLastInstalledFieldStorageDefinitions($field->getTargetEntityTypeId());
        if ($field_storage_definition[$field->getName()]) {
          \Drupal::entityDefinitionUpdateManager()->uninstallFieldStorageDefinition($field_storage_definition[$field->getName()]);
        }
        \Drupal::service('easy_entity_field.entity_update')->applyEntityUpdates($field->getTargetEntityTypeId());
      }
    }

    \Drupal::service('easy_entity_field.entity_update')->flushCached();
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();
    $definition = $this->getFieldTypeManager()->getDefinition($this->getFieldType());
    $this->addDependency('module', $definition['provider']);
    $this->addDependency('content', $this->getTargetEntityTypeId());
    // Plugins can declare additional dependencies in their definition.
    if (isset($definition['config_dependencies'])) {
      $this->addDependencies($definition['config_dependencies']);
    }

    return $this;
  }

  /**
   * Encapsulates the creation of the action's LazyPluginCollection.
   *
   * @return \Drupal\easy_entity_field\Plugin\EasyEntityFieldPluginCollection
   *   The action's plugin collection.
   */
  protected function getPluginCollection(): EasyEntityFieldPluginCollection {
    $this->pluginCollection = new EasyEntityFieldPluginCollection(
      \Drupal::service('plugin.manager.easy_entity_field'),
      $this->field_type,
      $this->getSettings()
    );

    return $this->pluginCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldPlugin(): mixed {
    return $this->getPluginCollection()->get($this->field_type);
  }

  /**
   * {@inheritdoc}
   */
  public function hasFieldPlugin(): bool {
    $plugin = \Drupal::service('plugin.manager.easy_entity_field');
    return !empty($plugin->hasDefinition($this->getFieldType()));
  }

  /**
   * Get Field Type Manager.
   *
   * @return mixed
   *   Get the schema from the field item class.
   */
  protected function getFieldTypeManager(): mixed {
    return \Drupal::service('plugin.manager.field.field_type');
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->field_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldType(): string {
    return $this->field_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string|TranslatableMarkup|null {
    return $this->label() ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $label) {
    $this->label = $label;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription(string $description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isRequired(): bool {
    return $this->required;
  }

  /**
   * {@inheritdoc}
   */
  public function setRequired(bool $required) {
    $this->required = $required;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings(): array {
    return array_merge($this->getFieldSettings(), $this->getStorageSettings());
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting($setting_name): array|string|null {
    $settings = $this->getSettings();
    if (array_key_exists($setting_name, $settings)) {
      return $settings[$setting_name];
    }
    else {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldSettings(): array {
    $field_settings = $this->getFieldTypeManager()->getDefaultFieldSettings($this->getFieldType());
    return $this->field_settings + $field_settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldSetting(string $setting_name): array|null {
    if (array_key_exists($setting_name, $this->field_settings)) {
      return $this->field_settings[$setting_name];
    }
    $settings = $this->getFieldSettings();
    if (array_key_exists($setting_name, $settings)) {
      return $settings[$setting_name];
    }
    else {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getStorageSettings(): array {
    $storage_settings = $this->getFieldTypeManager()->getDefaultStorageSettings($this->getFieldType());
    return $this->storage_settings + $storage_settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getStorageSetting($setting_name): array|null {
    if (array_key_exists($setting_name, $this->storage_settings)) {
      return $this->storage_settings[$setting_name];
    }
    $settings = $this->getStorageSettings();
    if (array_key_exists($setting_name, $settings)) {
      return $settings[$setting_name];
    }
    else {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldSettings(array $settings) {
    $this->field_settings = $settings + $this->field_settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setStorageSettings(array $settings) {
    $this->storage_settings = $settings + $this->storage_settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultValue(): array {
    return $this->default_value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultValue(array $values) {
    $this->default_value = $values;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldStorageDefinition() {
    return $this->getBaseFieldDefinition()->getFieldStorageDefinition();
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplay(string $display_name): array|string|null {
    return $this->display[$display_name] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function hasData(): bool {
    return \Drupal::entityTypeManager()->getStorage($this->entity_type)->countFieldData($this->getFieldStorageDefinition(), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFieldDefinition() {
    if (!isset($this->baseFieldDefinition)) {
      $fields = \Drupal::service('entity_field.manager')->getBaseFieldDefinitions($this->getTargetEntityTypeId());

      $this->baseFieldDefinition = $fields[$this->getName()];
    }
    return $this->baseFieldDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetEntityTypeId(string $entity_type_id) {
    $this->entity_type = $entity_type_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId(): string {
    return $this->entity_type ?? $this->getRouteParameter('entity_type_id');
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityType(): mixed {
    return $this->entityTypeManager()->getDefinition($this->getTargetEntityTypeId());
  }

  /**
   * {@inheritdoc}
   */
  public function isTargetEntityType(): bool {
    return !empty($this->entityTypeManager()->hasDefinition($this->getTargetEntityTypeId()));
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteParameter(string $parameter): mixed {
    return \Drupal::service('current_route_match')->getParameter($parameter);
  }

  /**
   * Verify if the field type has UI Definitions.
   *
   * @return bool
   *   Does it have a UI.
   */
  protected function isFieldUiDefinitions(): bool {
    return !empty($this->getFieldTypeManager()->getUiDefinitions()[$this->getFieldType()]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildBaseFieldDefinitions() {
    $entity_type = $this->entityTypeManager()->getDefinition($this->getTargetEntityTypeId());

    $storage_definition = BaseFieldDefinition::create($this->getFieldType())
      ->setLabel($this->t('@label', ['@label' => $this->getLabel()]))
      ->setTranslatable($entity_type->isTranslatable())
      ->setDescription($this->getDescription())
      ->setRevisionable($entity_type->isRevisionable())
      ->setSettings($this->getSettings());

    if ($this->isFieldUiDefinitions()) {
      $storage_definition->setDefaultValue($this->getDefaultValue())->setRequired($this->isRequired());

      if ($this->getDisplay('view')) {
        $storage_definition->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => $this->getDisplay('formatter_type'),
          'weight' => 3,
          'settings' => $this->getDisplay('formatter_settings'),
        ])->setDisplayConfigurable('view', TRUE);
      }

      if ($this->getDisplay('form')) {
        $storage_definition->setDisplayOptions('form', [
          'type' => $this->getDisplay('widget_type'),
          'weight' => 3,
          'settings' => $this->getDisplay('widget_settings'),
        ])->setDisplayConfigurable('form', TRUE);
      }
    }

    return $storage_definition;
  }

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'edit-form', array $options = []) {
    $base_route_name = "entity.{$this->getTargetEntityTypeId()}.base_field";
    $route_parameters = ['easy_entity_field' => $this->id()];
    $collection_route_name = $this->isTargetEntityType() ? $base_route_name : 'easy_entity_field.list_builder';
    $delete_route_name = $this->isTargetEntityType() ? $base_route_name . '.delete' : 'entity.easy_entity_field.delete_form';

    if ($rel == 'collection') {
      return URL::fromRoute($collection_route_name, $route_parameters, $options);
    }

    if ($rel == 'add-form') {
      return Url::fromRoute("{$base_route_name}.add", $route_parameters, $options);
    }

    if ($rel == 'storage-form') {
      return Url::fromRoute("{$base_route_name}.storage", $route_parameters, $options);
    }

    if ($rel == 'edit-form') {
      return Url::fromRoute("{$base_route_name}.edit", $route_parameters, $options);
    }

    if ($rel == 'delete-form') {
      return Url::fromRoute($delete_route_name, $route_parameters, $options);
    }
    return parent::toUrl($rel, $options);
  }

}
