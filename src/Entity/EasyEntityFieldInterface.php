<?php

namespace Drupal\easy_entity_field\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides an interface for defining Easy entity field entities.
 */
interface EasyEntityFieldInterface extends ConfigEntityInterface {

  /**
   * Returns the machine name of the field.
   *
   * This defines how the field data is accessed from the entity. For example,
   * if the field name is "foo", then $entity->foo returns its data.
   *
   * @return string
   *   The field name.
   */
  public function getName(): string;

  /**
   * Returns the field type.
   *
   * @return string
   *   The field type, i.e. the id of a field type plugin. For example 'text'.
   *
   * @see \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  public function getFieldType(): string;

  /**
   * Returns a Field label.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup|null
   *   The label. A string or an instance of TranslatableMarkup will be returned
   *   based on the way the label translation is handled. NULL if no label is
   *   available.
   */
  public function getLabel(): string|TranslatableMarkup|null;

  /**
   * Sets the Field label.
   *
   * @param string $label
   *   The label to set.
   *
   * @return $this
   */
  public function setLabel(string $label);

  /**
   * Returns the Field description.
   *
   * @return string
   *   The Field description.
   */
  public function getDescription(): string;

  /**
   * Sets the Field description.
   *
   * @param string $description
   *   The Field description.
   *
   * @return $this
   */
  public function setDescription(string $description);

  /**
   * Returns whether the field can be empty.
   *
   * If a field is required, an entity needs to have at least a valid,
   * non-empty item in that field's FieldItemList in order to pass validation.
   *
   * An item is considered empty if its isEmpty() method returns TRUE.
   * Typically, that is if at least one of its required properties is empty.
   *
   * @return bool
   *   TRUE if the field is required.
   */
  public function isRequired(): bool;

  /**
   * Sets whether the field can be empty.
   *
   * If a field is required, an entity needs to have at least a valid,
   * non-empty item in that field's FieldItemList in order to pass validation.
   *
   * An item is considered empty if its isEmpty() method returns TRUE.
   * Typically, that is if at least one of its required properties is empty.
   *
   * @param bool $required
   *   TRUE if the field is required. FALSE otherwise.
   *
   * @return $this
   *   The current object, for a fluent interface.
   */
  public function setRequired(bool $required);

  /**
   * Gets the plugin for the field type.
   *
   * @return mixed
   *   Get the plugin for the field.
   */
  public function getFieldPlugin(): mixed;

  /**
   * Plugin for verifying field types.
   *
   * @return bool
   *   TRUE if the field has a field plugin.
   */
  public function hasFieldPlugin(): bool;

  /**
   * Get all field settings and field storage settings.
   *
   * @return array
   *   An associative array of settings.
   */
  public function getSettings(): array;

  /**
   * Get all field setting and field storage setting.
   *
   * @return array|string|null
   *   An associative array of setting.
   */
  public function getSetting($setting_name): array|string|null;

  /**
   * Get all field settings.
   *
   * @return array
   *   An associative array of field settings.
   */
  public function getFieldSettings(): array;

  /**
   * Get all field storage settings.
   *
   * @return array
   *   An associative array of field storage settings.
   */
  public function getStorageSettings(): array;

  /**
   * Gets the value set for a certain field.
   *
   * @param string $setting_name
   *   An associative array of field setting.
   *
   * @return array|null
   *   The value of the field setting.
   */
  public function getFieldSetting(string $setting_name): array|null;

  /**
   * Gets the value of a field storage setting.
   *
   * @param string $setting_name
   *   An associative array of field storage setting.
   *
   * @return array|null
   *   The value of the field storage setting.
   */
  public function getStorageSetting(string $setting_name): array|null;

  /**
   * Sets field settings.
   *
   * @param array $settings
   *   The array of field settings.
   *
   * @return $this
   */
  public function setFieldSettings(array $settings);

  /**
   * Sets field Storage settings.
   *
   * @param array $settings
   *   The array of field settings.
   *
   * @return $this
   *   Set Field Storage Settings.
   */
  public function setStorageSettings(array $settings);

  /**
   * Returns the default value for the field in a newly created entity.
   *
   * This method computes the runtime default value for a field in a given
   * entity. To access the raw properties assigned to the field definition,
   * ::getDefaultValueLiteral() or ::getDefaultValueCallback() should be used
   * instead.
   *
   * @return array
   *   The default value for the field, as a numerically indexed array of items,
   *   each item being a property/value array (array() for no default value).
   */
  public function getDefaultValue(): array;

  /**
   * Sets the default data value.
   *
   * @param mixed $values
   *   The default value to be set or NULL to remove any default value.
   *
   * @return $this
   *   The default value for the field, as a numerically indexed array of items,
   */
  public function setDefaultValue(array $values);

  /**
   * Returns the field storage definition.
   *
   * @return \Drupal\Core\Field\FieldStorageDefinitionInterface
   *   The field storage definition.
   */
  public function getFieldStorageDefinition();

  /**
   * Gets the current display plugin.
   *
   * @param string $display_name
   *   The current display plugin.
   *
   * @return array|string|null
   *   The display plugin definition.
   */
  public function getDisplay(string $display_name): array|string|null;

  /**
   * Determines if the storage contains any data.
   *
   * @return bool
   *   TRUE if the storage contains data, FALSE if not.
   */
  public function hasData(): bool;

  /**
   * Sets the ID of the type of the entity this field is attached to.
   *
   * @param string $entity_type_id
   *   The name of the target entity type to set.
   *
   * @return string
   *   The entity type ID.
   */
  public function setTargetEntityTypeId(string $entity_type_id);

  /**
   * Gets the entity type for which this display is used.
   *
   * @return string
   *   The entity type id.
   */
  public function getTargetEntityTypeId(): string;

  /**
   * Gets the entity type for which this display is used.
   *
   * @return mixed
   *   The entity type.
   */
  public function getTargetEntityType(): mixed;

  /**
   * Verify the entity type using this display.
   *
   * @return bool
   *   If the entity type exists, it is true; otherwise, it is false.
   */
  public function isTargetEntityType(): bool;

  /**
   * Returns the route parameters needed to render a link for the action.
   *
   * @param string $parameter
   *   Parameters of the route.
   *
   * @return mixed
   *   The route parameter.
   */
  public function getRouteParameter(string $parameter): mixed;

  /**
   * Gets the base field definition.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   The base field definition.
   */
  public function getBaseFieldDefinition();

  /**
   * Builds base field definitions for an entity type.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of field definitions, keyed by field name.
   */
  public function buildBaseFieldDefinitions();

}
