<?php

namespace Drupal\easy_entity_field\EventSubscriber;

use Drupal\Core\Entity\EntityTypeEventSubscriberTrait;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Easy entity base field event subscriber.
 */
class EasyEntityFieldSubscriber implements EventSubscriberInterface {

  use EntityTypeEventSubscriberTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs a new FieldUiPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return static::getEntityTypeEvents();
  }

  /**
   * {@inheritdoc}
   */
  public function onEntityTypeDelete(EntityTypeInterface $entity_type): void {
    $easy_entity_fields = $this->entityTypeManager->getStorage('easy_entity_field')->loadMultiple();
    $settings = $this->configFactory->listAll('easy_entity_field.settings.');
    $setting_id = 'easy_entity_field.settings.' . $entity_type->id();
    foreach ($settings as $setting) {
      if ($setting == $setting_id) {
        $this->configFactory->getEditable($setting_id)->delete();
      }
    }

    /** @var \Drupal\easy_entity_field\Entity\EasyEntityFieldInterface $easy_entity_field */
    foreach ($easy_entity_fields as $easy_entity_field) {
      if ($easy_entity_field->getTargetEntityTypeId() == $entity_type->id()) {
        $easy_entity_field->delete();
      }
    }
  }

}
