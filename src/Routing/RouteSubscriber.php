<?php

namespace Drupal\easy_entity_field\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\easy_entity_field\EasyEntityFieldSettingsInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Define the route subscriber.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The easy entity field settings.
   *
   * @var \Drupal\easy_entity_field\EasyEntityFieldSettingsInterface
   */
  protected EasyEntityFieldSettingsInterface $fieldSettings;

  /**
   * Constructs a FieldUiLocalAction object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\easy_entity_field\EasyEntityFieldSettingsInterface $field_settings
   *   This is the field setting.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EasyEntityFieldSettingsInterface $field_settings) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldSettings = $field_settings;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    foreach ($this->fieldSettings->getEntityTypes() as $entity_type_id => $settings) {
      if ($settings['base_route'] && !empty($collection->get($settings['base_route']))) {
        $entity_route = $collection->get($settings['base_route']);
        $entity_path = $entity_route->getPath();
        $entity_options = $entity_route->getOptions();
      }
      else {
        $entity_path = '/admin/config/development/easy-entity-field';
        $entity_options = [];
      }

      $defaults = [
        'entity_type_id' => $entity_type_id,
        'target_entity_type_id' => $entity_type_id,
      ];

      $entity_options['parameters'][$entity_type_id] = [
        'type' => 'entity:' . $entity_type_id,
      ];

      $entity_options['_field_ui'] = TRUE;

      $route = new Route("$entity_path/base-field");
      $route->setDefaults(
        [
          '_entity_list' => 'easy_entity_field',
          '_title' => 'Manage Base Fields',
        ] + $defaults
      )->setRequirements([
        '_permission' => 'administer ' . $entity_type_id . ' base fields',
      ])->setOptions($entity_options);
      $collection->add("entity.{$entity_type_id}.base_field", $route);

      $route = new Route("$entity_path/base-field/add");
      $route->setDefaults(
        [
          '_title' => 'Create a new base field',
          '_form' => '\Drupal\easy_entity_field\Form\EasyEntityFieldAddForm',
        ] + $defaults
      )->setRequirements([
        '_permission' => 'administer ' . $entity_type_id . ' base fields',
      ])->setOptions($entity_options);
      $collection->add("entity.{$entity_type_id}.base_field.add", $route);

      $route = new Route("$entity_path/base-field/{easy_entity_field}/storage");
      $route->setDefaults(
        [
          '_title' => 'Field settings',
          '_entity_form' => 'easy_entity_field.storage',
        ] + $defaults
      )->setRequirements([
        '_permission' => 'administer ' . $entity_type_id . ' base fields',
      ])->setOptions($entity_options);
      $collection->add("entity.{$entity_type_id}.base_field.storage", $route);

      $route = new Route("$entity_path/base-field/{easy_entity_field}");
      $route->setDefaults(
        [
          '_title' => 'Edit base field',
          '_entity_form' => 'easy_entity_field.edit',
        ] + $defaults
      )->setRequirements([
        '_permission' => 'administer ' . $entity_type_id . ' base fields',
      ])->setOptions($entity_options);
      $collection->add("entity.{$entity_type_id}.base_field.edit", $route);

      $route = new Route("$entity_path/base-field/{easy_entity_field}/delete");
      $route->setDefaults(
        [
          '_title' => 'Delete base field',
          '_entity_form' => 'easy_entity_field.delete',
        ] + $defaults
      )->setRequirements([
        '_permission' => 'administer ' . $entity_type_id . ' base fields',
      ])->setOptions($entity_options);
      $collection->add("entity.{$entity_type_id}.base_field.delete", $route);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -100];
    return $events;
  }

}
