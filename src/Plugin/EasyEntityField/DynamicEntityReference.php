<?php

namespace Drupal\easy_entity_field\Plugin\EasyEntityField;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dynamic_entity_reference\Plugin\Field\FieldType\DynamicEntityReferenceItem;
use Drupal\easy_entity_field\Plugin\EasyEntityFieldBase;

/**
 * Defines the 'dynamic_entity_reference' entity field type.
 *
 * @EasyEntityField(
 *   id = "dynamic_entity_reference",
 *   label = @Translation("Dynamic entity reference"),
 *   description = @Translation("An entity field containing a dynamic entity reference.")
 * )
 */
class DynamicEntityReference extends EasyEntityFieldBase {

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {
    // @todo inject this.
    $labels = \Drupal::service('entity_type.repository')->getEntityTypeLabels(TRUE);

    /**
     * @var \Drupal\easy_entity_field\Entity\EasyEntityFieldInterface $easy_entity_field
     */
    $easy_entity_field = $form_state->getFormObject()->getEntity();

    $element['exclude_entity_types'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude the selected items'),
      '#default_value' => $easy_entity_field->getSetting('exclude_entity_types'),
      '#disabled' => $has_data,
    ];

    $element['entity_type_ids'] = [
      '#type' => 'select',
      '#title' => $this->t('Select items'),
      '#options' => $labels[(string) $this->t('Content', [], ['context' => 'Entity type group'])],
      '#default_value' => $easy_entity_field->getSetting('entity_type_ids'),
      '#disabled' => $has_data,
      '#multiple' => TRUE,
      '#element_validate' => [
        [DynamicEntityReferenceItem::class, 'storageSettingsFormValidate'],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    /**
     * @var \Drupal\easy_entity_field\Entity\EasyEntityFieldInterface $easy_entity_field
     */
    $easy_entity_field = $form_state->getFormObject()->getEntity();

    $settings_form = [];
    $settings = $easy_entity_field->getSettings();
    // Config entities are excluded from the UI.
    foreach (DynamicEntityReferenceItem::getTargetTypes($settings, FALSE) as $target_type) {
      $entity_type = \Drupal::entityTypeManager()->getDefinition($target_type);
      $settings_form[$target_type] = $this->targetTypeFieldSettingsForm($form, $form_state, $target_type);
      $settings_form[$target_type]['handler']['#title'] = $this->t('Reference type for @target_type', ['@target_type' => $entity_type->getLabel()]);
    }
    return $settings_form;
  }

  /**
   * Returns a form for single target type settings.
   *
   * This is same as
   * \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem::fieldSettingsForm()
   * but it uses dynamic_entity_reference_selection plugin manager instead of
   * entity_reference_selection plugin manager.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   * @param string $target_type
   *   The target entity type id.
   *
   * @return array
   *   The form definition for the field settings.
   */
  protected function targetTypeFieldSettingsForm(array $form, FormStateInterface $form_state, string $target_type): array {
    /**
     * @var \Drupal\easy_entity_field\Entity\EasyEntityFieldInterface $easy_entity_field
     */
    $easy_entity_field = $form_state->getFormObject()->getEntity();
    $field_settings = $easy_entity_field->getSettings();
    /** @var \Drupal\dynamic_entity_reference\SelectionPluginManager $manager */
    $manager = \Drupal::service('plugin.manager.dynamic_entity_reference_selection');
    // Get all selection plugins for this entity type.
    $selection_plugins = $manager->getSelectionGroups($target_type);
    $handlers_options = [];
    foreach (array_keys($selection_plugins) as $selection_group_id) {
      // We only display base plugins (e.g. 'default', 'views', ...) and not
      // entity type specific plugins (e.g. 'default:node', 'default:user',
      // ...).
      if (array_key_exists($selection_group_id, $selection_plugins[$selection_group_id])) {
        $handlers_options[$selection_group_id] = Html::escape($selection_plugins[$selection_group_id][$selection_group_id]['label']);
      }
      elseif (array_key_exists($selection_group_id . ':' . $target_type, $selection_plugins[$selection_group_id])) {
        $selection_group_plugin = $selection_group_id . ':' . $target_type;
        $handlers_options[$selection_group_plugin] = Html::escape($selection_plugins[$selection_group_id][$selection_group_plugin]['base_plugin_label']);
      }
    }

    $form = [
      '#type' => 'container',
      '#process' => [[EntityReferenceItem::class, 'fieldSettingsAjaxProcess']],
      '#element_validate' => [
        [
          DynamicEntityReferenceItem::class,
          'fieldSettingsFormValidate',
        ],
      ],
    ];
    $form['handler'] = [
      '#type' => 'details',
      '#title' => $this->t('Reference type'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#process' => [[EntityReferenceItem::class, 'formProcessMergeParent']],
    ];

    $form['handler']['handler'] = [
      '#type' => 'select',
      '#title' => $this->t('Reference method'),
      '#options' => $handlers_options,
      '#default_value' => $field_settings[$target_type]['handler'],
      '#required' => TRUE,
      '#ajax' => TRUE,
      '#limit_validation_errors' => [],
    ];
    $form['handler']['handler_submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Change handler'),
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['js-hide'],
      ],
      '#submit' => ['entity_reference_settings_ajax_submit'],
    ];

    $form['handler']['handler_settings'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entity_reference-settings']],
    ];

    $settings = $easy_entity_field->getSettings();
    $options = $settings[$target_type]['handler_settings'] ?? [];
    $options += [
      'target_type' => $target_type,
      'handler' => $settings[$target_type]['handler'],
      'entity' => NULL,
    ];

    $selection_handler = $manager->getInstance($options);

    $form['handler']['handler_settings'] += $selection_handler->buildConfigurationForm([], $form_state);

    return $form;
  }

}
