<?php

namespace Drupal\easy_entity_field\Plugin\EasyEntityField;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\easy_entity_field\Plugin\EasyEntityFieldBase;

/**
 * Defines the 'entity_reference' entity field type.
 *
 * @EasyEntityField(
 *   id = "entity_reference",
 *   label = @Translation("Entity reference"),
 *   description = @Translation("An entity field containing an entity reference.")
 * )
 */
class EntityReference extends EasyEntityFieldBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    /**
     * @var \Drupal\easy_entity_field\Entity\EasyEntityFieldInterface $easy_entity_field
     */
    $easy_entity_field = $form_state->getFormObject()->getEntity();

    // Get all selection plugins for this entity type.
    $selection_plugins = \Drupal::service('plugin.manager.entity_reference_selection')->getSelectionGroups($easy_entity_field->getSetting('target_type'));
    $handlers_options = [];
    foreach (array_keys($selection_plugins) as $selection_group_id) {
      // We only display base plugins (e.g. 'default', 'views', ...) and not
      // entity type specific plugins (e.g. 'default:node', 'default:user',
      // ...).
      if (array_key_exists($selection_group_id, $selection_plugins[$selection_group_id])) {
        $handlers_options[$selection_group_id] = Html::escape($selection_plugins[$selection_group_id][$selection_group_id]['label']);
      }
      elseif (array_key_exists($selection_group_id . ':' . $easy_entity_field->getSetting('target_type'), $selection_plugins[$selection_group_id])) {
        $selection_group_plugin = $selection_group_id . ':' . $easy_entity_field->getSetting('target_type');
        $handlers_options[$selection_group_plugin] = Html::escape($selection_plugins[$selection_group_id][$selection_group_plugin]['base_plugin_label']);
      }
    }

    $form = [
      '#type' => 'container',
      '#process' => [[EntityReferenceItem::class, 'fieldSettingsAjaxProcess']],
      '#element_validate' => [[static::class, 'fieldSettingsFormValidate']],
    ];

    $form['handler'] = [
      '#type' => 'details',
      '#title' => $this->t('Reference type'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#process' => [[EntityReferenceItem::class, 'formProcessMergeParent']],
    ];

    $form['handler']['handler'] = [
      '#type' => 'select',
      '#title' => $this->t('Reference method'),
      '#options' => $handlers_options,
      '#default_value' => $easy_entity_field->getSetting('handler'),
      '#required' => TRUE,
      '#ajax' => TRUE,
      '#limit_validation_errors' => [],
    ];

    $form['handler']['handler_submit'] = [
      '#type' => 'submit',
      '#name' => 'handler_settings_submit',
      '#value' => $this->t('Change handler'),
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['js-hide'],
      ],
      '#submit' => [[EntityReferenceItem::class, 'settingsAjaxSubmit']],
    ];

    $form['handler']['handler_settings'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entity_reference-settings']],
    ];

    $manager = \Drupal::service('plugin.manager.entity_reference_selection');
    $options = [
      'target_type' => $easy_entity_field->getSetting('target_type'),
      'handler' => $easy_entity_field->getSetting('handler'),
      'entity' => $easy_entity_field,
    ] + $easy_entity_field->getSetting('handler_settings') ?: [];

    $selection_handler = $manager->getInstance($options);
    $form['handler']['handler_settings'] += $selection_handler->buildConfigurationForm([], $form_state);

    return $form;
  }

  /**
   * Form element validation handler; Invokes selection plugin's validation.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   */
  public static function fieldSettingsFormValidate(array $form, FormStateInterface $form_state): void {
    /**
     * @var \Drupal\easy_entity_field\Entity\EasyEntityFieldInterface $easy_entity_field
     */
    $easy_entity_field = $form_state->getFormObject()->getEntity();
    $manager = \Drupal::service('plugin.manager.entity_reference_selection');

    $options = [
      'target_type' => $easy_entity_field->getSetting('target_type'),
      'handler' => $easy_entity_field->getSetting('handler'),
      'entity' => NULL,
    ] + $easy_entity_field->getSetting('handler_settings') ?: [];

    $selection_handler = $manager->getInstance($options);
    $selection_handler->validateConfigurationForm($form, $form_state);
  }

}
