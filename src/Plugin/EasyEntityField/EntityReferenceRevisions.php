<?php

namespace Drupal\easy_entity_field\Plugin\EasyEntityField;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines the 'entity_reference_revisions' entity field type.
 *
 * @EasyEntityField(
 *   id = "entity_reference_revisions",
 *   label = @Translation("Entity reference revisions"),
 *   description = @Translation("An entity field containing an entity reference to a specific revision.")
 * )
 */
class EntityReferenceRevisions extends EntityReference {

  use StringTranslationTrait;
}
