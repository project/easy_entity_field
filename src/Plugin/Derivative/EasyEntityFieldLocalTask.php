<?php

namespace Drupal\easy_entity_field\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\easy_entity_field\EasyEntityFieldSettingsInterface;

/**
 * Create easy entity base field local task for entity types.
 */
class EasyEntityFieldLocalTask extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The easy entity field settings.
   *
   * @var \Drupal\easy_entity_field\EasyEntityFieldSettingsInterface
   */
  protected EasyEntityFieldSettingsInterface $fieldSettings;

  /**
   * Constructs a FieldUiLocalAction object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\easy_entity_field\EasyEntityFieldSettingsInterface $field_settings
   *   This is the field setting.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EasyEntityFieldSettingsInterface $field_settings) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldSettings = $field_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('easy_entity_field.settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $this->derivatives = [];

    foreach ($this->fieldSettings->getEntityTypes() as $settings) {
      $base_route_name = $settings['base_route'];

      if (!isset($base_route_name)) {
        continue;
      }
      if ($settings['local_task']) {
        $this->derivatives[$base_route_name] = [
          'route_name' => $base_route_name,
          'weight' => -10,
          'title' => $this->t('List'),
          'base_route' => $base_route_name,
        ];
      }
    }

    foreach ($this->fieldSettings->getEntityTypes() as $entity_type_id => $settings) {
      $base_route_name = $settings['base_route'];

      if (!isset($base_route_name)) {
        continue;
      }

      $this->derivatives["easy_entity_field_default.$entity_type_id"] = [
        'route_name' => "entity.$entity_type_id.base_field",
        'weight' => 8,
        'title' => $this->t('Manage Base Fields'),
        'base_route' => $base_route_name,
      ];

      $this->derivatives["easy_entity_field_edit.$entity_type_id"] = [
        'route_name' => "entity.$entity_type_id.base_field.edit",
        'title' => $this->t('Edit'),
        'base_route' => "entity.$entity_type_id.base_field.edit",
      ];

      $this->derivatives["easy_entity_field_storage.$entity_type_id"] = [
        'route_name' => "entity.$entity_type_id.base_field.storage",
        'title' => $this->t('Field settings'),
        'base_route' => "entity.$entity_type_id.base_field.edit",
      ];
    }

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
