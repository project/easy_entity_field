<?php

namespace Drupal\easy_entity_field\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\easy_entity_field\EasyEntityFieldSettingsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide local action definitions for all enabled entity types.
 */
class EasyEntityFieldLocalAction extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The easy entity field settings.
   *
   * @var \Drupal\easy_entity_field\EasyEntityFieldSettingsInterface
   */
  protected EasyEntityFieldSettingsInterface $fieldSettings;

  /**
   * Constructs a FieldUiLocalAction object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\easy_entity_field\EasyEntityFieldSettingsInterface $field_settings
   *   This is the field setting.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EasyEntityFieldSettingsInterface $field_settings) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldSettings = $field_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('easy_entity_field.settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $this->derivatives = [];

    foreach ($this->fieldSettings->getEntityTypes() as $settings) {
      $entity_type_id = $settings['entity_type_id'];
      $this->derivatives["base_field_add.$entity_type_id"] = [
        'route_name' => "entity.$entity_type_id.base_field.add",
        'title' => $this->t('Create a new base field'),
        'appears_on' => ["entity.$entity_type_id.base_field"],
      ];
    }

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
