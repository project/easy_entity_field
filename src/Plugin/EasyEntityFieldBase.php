<?php

namespace Drupal\easy_entity_field\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base class for Easy entity field plugins.
 */
abstract class EasyEntityFieldBase extends PluginBase implements EasyEntityFieldPluginInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultValuesForm(array $form, FormStateInterface $form_state): array {
    return [];
  }

}
