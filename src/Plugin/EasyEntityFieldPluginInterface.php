<?php

namespace Drupal\easy_entity_field\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines an interface for Easy entity field plugins.
 */
interface EasyEntityFieldPluginInterface extends PluginInspectionInterface {

  /**
   * Returns a form for the storage-level settings.
   *
   * Invoked from \Drupal\field_ui\Form\FieldStorageConfigEditForm to allow
   * administrators to configure storage-level settings.
   *
   * Field storage might reject settings changes that affect the field
   * storage schema if the storage already has data. When the $has_data
   * parameter is TRUE, the form should not allow changing the settings that
   * take part in the schema() method. It is recommended to set #access to
   * FALSE on the corresponding elements.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   * @param bool $has_data
   *   TRUE if the field already has data, FALSE if not.
   *
   * @return array
   *   The form definition for the field settings.
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array;

  /**
   * Returns a form for the field-level settings.
   *
   * Invoked from \Drupal\field_ui\Form\FieldConfigEditForm to allow
   * administrators to configure field-level settings.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   *
   * @return array
   *   The form definition for the field settings.
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array;

  /**
   * Returns a form for the default value input.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form definition for the field default value.
   */
  public function defaultValuesForm(array $form, FormStateInterface $form_state): array;

}
