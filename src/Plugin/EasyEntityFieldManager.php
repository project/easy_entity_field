<?php

namespace Drupal\easy_entity_field\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides the Easy entity field plugin manager.
 */
class EasyEntityFieldManager extends DefaultPluginManager {

  use StringTranslationTrait;

  /**
   * Constructs a new EasyEntityFieldManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/EasyEntityField', $namespaces, $module_handler, 'Drupal\easy_entity_field\Plugin\EasyEntityFieldPluginInterface', 'Drupal\easy_entity_field\Annotation\EasyEntityField');
    $this->alterInfo('easy_entity_field_info');
    $this->setCacheBackend($cache_backend, 'easy_entity_field_plugins');
  }

}
