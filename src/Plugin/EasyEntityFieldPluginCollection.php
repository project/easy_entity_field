<?php

namespace Drupal\easy_entity_field\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;

/**
 * A collection of base field configs.
 */
class EasyEntityFieldPluginCollection extends DefaultSingleLazyPluginCollection {

  public function __construct(PluginManagerInterface $manager, $instance_id, array $configuration) {
    parent::__construct($manager, $instance_id, $configuration);
    $this->manager = $manager;
    if ($instance_id !== NULL) {
      $this->addInstanceId($instance_id, $configuration);
    }
  }

}
