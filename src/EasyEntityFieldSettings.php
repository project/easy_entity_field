<?php

namespace Drupal\easy_entity_field;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions of the field_ui module.
 */
class EasyEntityFieldSettings implements EasyEntityFieldSettingsInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs a new FieldUiPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypes(): array {
    $settings = [];

    $configs = $this->configFactory->listAll('easy_entity_field.settings.');
    foreach ($configs as $config_id) {
      $setting = $this->configFactory->getEditable($config_id);
      if ($setting->get('status') && $entity_type_id = $setting->get('entity_type_id')) {
        $settings[$entity_type_id]['entity_type_id'] = $entity_type_id;
        $settings[$entity_type_id]['status'] = $setting->get('status');
        $settings[$entity_type_id]['base_route'] = $setting->get('base_route');
        $settings[$entity_type_id]['local_task'] = $setting->get('local_task');
      }
    }

    return $settings;
  }

}
