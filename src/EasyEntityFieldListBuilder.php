<?php

namespace Drupal\easy_entity_field;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Easy entity field entities.
 */
class EasyEntityFieldListBuilder extends ConfigEntityListBuilder {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The current route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $currentRouteMatch;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Get a list of fields for the current entity type.
   *
   * @var bool
   */
  protected $current_list;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The current route match service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    RouteMatchInterface $current_route_match,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct(
      $entity_type,
      $entity_type_manager->getStorage($entity_type->id())
    );
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRouteMatch = $current_route_match;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container,
    EntityTypeInterface $entity_type
  ) {
    return new static(
      $entity_type,
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render($list = FALSE): array {
    if ($list) {
      $this->current_list = $list;
    }
    $build = parent::render();
    $build['table']['#attributes']['id'] = 'entity-base-field-overview';
    $build['table']['#empty'] = $this->t('There are currently no custom base fields.');
    return $build;
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds(): array {
    $query = $this->getStorage()->getQuery();

    if (!$this->current_list && $base_entity_type_id = $this->getTargetEntityTypeId()) {
      $query->condition('entity_type', $base_entity_type_id);
    }

    $query->sort($this->entityType->getKey('id'));
    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager(50);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Field name');
    $header['field_name'] = $this->t('Machine name');
    $header['field_type'] = $this->t('Field type');
    if (!empty($this->current_list)) {
      $header['entity_type'] = $this->t('Entity type');
    }
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['label'] = $entity->label();
    $row['field_name'] = $entity->getName();
    $row['field_type'] = $entity->getFieldType();
    if (!empty($this->current_list)) {
      $entity_type_label = !empty($entity->isTargetEntityType()) ? $entity->getTargetEntityType()->getLabel() : $this->t('Target entity type has been deleted');
      $row['entity_type'] = $this->t('%entity_type_label', ['%entity_type_label' => $entity_type_label]);
    }
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity): array {
    /** @var \Drupal\easy_entity_field\Entity\EasyEntityField $entity */

    $operations = parent::getDefaultOperations($entity);
    if ($this->configFactory->getEditable("easy_entity_field.settings.{$entity->getTargetEntityTypeId()}")->get('status')
      && !empty($entity->isTargetEntityType())
      && $entity->access('update')
    ) {
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'weight' => 10,
        'url' => $entity->toUrl('edit-form'),
      ];

      $operations['storage'] = [
        'title' => $this->t('Field settings'),
        'weight' => 10,
        'url' => $entity->toUrl('storage-form'),
      ];

      if ($entity->access('delete')) {
        $operations['delete'] = [
          'title' => $this->t('Delete'),
          'weight' => 100,
          'url' => $entity->toUrl('delete-form'),
          'attributes' => [
            'class' => ['use-ajax'],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode([
              'width' => 880,
            ]),
          ],
        ];
      }
    }
    else {
      if ($entity->access('delete')) {
        $field_ui_url = new Url("entity.easy_entity_field.delete_form", [
          'easy_entity_field' => $entity->id(),
          'orphaned_field' => TRUE,
        ]);
        $operations['delete'] = [
          'title' => $this->t('Delete Orphaned Field'),
          'weight' => 10,
          'attributes' => [
            'class' => ['use-ajax'],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode([
              'width' => 880,
            ]),
          ],
          'url' => $this->ensureDestination($field_ui_url),
        ];
      }
    }

    return $operations;
  }

  /**
   * Get base entity type identifier.
   *
   * @return string|null
   *   The base entity type identifier.
   */
  protected function getTargetEntityTypeId(): ?string {
    return $this->currentRouteMatch->getParameter('entity_type_id');
  }

}
