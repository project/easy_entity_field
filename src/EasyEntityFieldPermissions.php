<?php

namespace Drupal\easy_entity_field;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions of the field_ui module.
 */
class EasyEntityFieldPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The easy entity field settings.
   *
   * @var \Drupal\easy_entity_field\EasyEntityFieldSettingsInterface
   */
  protected EasyEntityFieldSettingsInterface $fieldSettings;

  /**
   * Constructs a FieldUiLocalAction object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\easy_entity_field\EasyEntityFieldSettingsInterface $field_settings
   *   Enable entity data for easy entity fields.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EasyEntityFieldSettingsInterface $field_settings) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldSettings = $field_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('easy_entity_field.settings')
    );
  }

  /**
   * Returns an array of field UI permissions.
   *
   * @return array
   *   The field UI permissions.
   */
  public function fieldPermissions(): array {
    $permissions = [];

    foreach ($this->fieldSettings->getEntityTypes() as $settings) {
      $entity_type_id = $settings['entity_type_id'];
      if ($entity_type = $this->entityTypeManager->getDefinition($entity_type_id)) {
        $dependencies = ['module' => [$entity_type->getProvider()]];
        // Create a permission for each fieldable entity to manage
        // the fields and the display.
        $permissions['administer ' . $entity_type_id . ' base fields'] = [
          'title' => $this->t('%entity_label: Administer base fields', ['%entity_label' => $entity_type->getLabel()]),
          'restrict access' => TRUE,
          'dependencies' => $dependencies,
        ];
      }
    }

    return $permissions;
  }

}
