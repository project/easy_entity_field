<?php

namespace Drupal\easy_entity_field\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Set and enable entity fields for ease of use.
 */
class EasyEntityFieldSettingsForm extends FormBase {

  /**
   * The Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected ModuleExtensionList $moduleExtensionList;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Constructs a new EasyEntityFieldSettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ModuleExtensionList $extension_list_module, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleExtensionList = $extension_list_module;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('extension.list.module'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'easy_entity_field_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    /** @var \Symfony\Component\Routing\RouteCollection $route_collection */
    $route_collection = \Drupal::service('router')->getRouteCollection();

    // Load an array of entity types to remove from the JSON file.
    $remove_entity_type_file = $this->moduleExtensionList->getPath('easy_entity_field') . "/entity_type_remove.json";

    $entity_types = [];
    if (file_exists($remove_entity_type_file)) {
      $entity_types = Json::decode(file_get_contents($remove_entity_type_file));
    }

    // Entity type to be removed from the hook.
    $hook = 'easy_entity_field_remove_entity_type';
    if ($this->moduleHandler->hasImplementations($hook)) {
      $entity_types = NestedArray::mergeDeep($entity_types, $this->moduleHandler->invokeAll($hook, [$entity_types]));
    }

    // Get all content entity types and reorganize the array.
    $entity_type_content = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
      if ($entity_type instanceof ContentEntityTypeInterface && empty($entity_types[$entity_type->id()])) {
        $entity_type_content[$entity_type->id()] = $entity_type;
      }
    }

    $form['entity_types'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('Status'),
        $this->t('Manage'),
      ],
      '#tree' => TRUE,
    ];

    foreach ($entity_type_content as $entity_type_id => $entity_type) {
      $settings = $this->configFactory()->getEditable("easy_entity_field.settings.$entity_type_id");

      $form['entity_types'][$entity_type_id]['label'] = [
        '#markup' => $this->t('@label', ['@label' => $entity_type->getLabel()]),
      ];

      $form['entity_types'][$entity_type_id]['status'] = [
        '#type' => 'checkbox',
        '#default_value' => $settings->get('status') ?? '',
      ];

      // Entity Field Management Link.
      if ($settings->get('status') && $route_collection->get("entity.$entity_type_id.base_field")) {
        $links = [];

        $links['manage_base_fields'] = [
          'title' => $this->t('Manage'),
          'url' => Url::fromRoute("entity.$entity_type_id.base_field", [], ['attributes' => ['target' => '_blank']]),
        ];

        $form['entity_types'][$entity_type_id]['manage'] = [
          '#type' => 'operations',
          '#links' => $links,
        ];
      }
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $form_state->cleanValues();

    $route_name = '';
    foreach ($form_state->getValue('entity_types') as $entity_type_id => $values) {
      if ($values['status']) {
        $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
        if (!empty($entity_type->getBundleEntityType())) {
          if ($entity_type->get('easy_entity_type')) {
            $route_name = "entity.easy_entity_type.{$entity_type_id}.edit_form";
          }
          else {
            $bundle = $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType());
            if ($bundle->getLinkTemplate('collection')) {
              $url = Url::fromUserInput($bundle->getLinkTemplate('collection'));
              $route_name = $url->getRouteName();
              if ($entity_type_id == 'group') {
                $route_name = 'entity.group.collection';
              }
            }
          }
        }
        else {
          $route_name = $entity_type->get('field_ui_base_route');
        }

        $values['base_route'] = $route_name;
        $values['entity_type_id'] = $entity_type_id;

        // Determine the local task status.
        $local_tasks = \Drupal::service('plugin.manager.menu.local_task');
        if ($route_name && empty($local_tasks->getLocalTasksForRoute($route_name)[0])) {
          $local_task = TRUE;
        }
        else {
          $local_task = FALSE;
        }
        $values['local_task'] = $local_task;

        $settings = $this->configFactory()->getEditable("easy_entity_field.settings.{$entity_type_id}");
        $settings->setData($values)->save();
      }
      else {
        $this->configFactory()->getEditable("easy_entity_field.settings.{$entity_type_id}")->delete();
      }
    }

    // Clean up the relevant cache.
    \Drupal::service('easy_entity_field.entity_update')->flushCached();
  }

}
