<?php

namespace Drupal\easy_entity_field\Form;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Field\FallbackFieldTypeCategory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field_ui\Form\FieldStorageAddForm;

/**
 * Provides a form for the "field storage" add page.
 *
 * @internal
 */
class EasyEntityFieldAddForm extends FieldStorageAddForm {

  /**
   * The name of the entity type.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL, $bundle = NULL): array {
    if (!$form_state->get('entity_type_id')) {
      $form_state->set('entity_type_id', $entity_type_id);
    }

    $this->entityTypeId = $form_state->get('entity_type_id');
    $form = parent::buildForm($form, $form_state);

    // Fields to be removed in the option list.
    $field_type_removed = [
      'file' => TRUE,
    ];

    // Fields to be removed from the hook.
    $hook = 'easy_entity_field_remove_field_type';
    if (\Drupal::moduleHandler()->hasImplementations($hook)) {
      $field_type_removed = NestedArray::mergeDeep($field_type_removed, \Drupal::moduleHandler()->invokeAll($hook, [$field_type_removed]));
    }

    $field_type_options = $unique_definitions = [];
    $definitions = $this->fieldTypePluginManager->getUiDefinitions() + $this->fieldTypePluginManager->getDefinitions();
    \Drupal::moduleHandler()->alter('field_info_entity_type_ui_definitions', $definitions, $this->entityTypeId);
    $grouped_definitions = $this->fieldTypePluginManager->getGroupedDefinitions($definitions, 'label', 'id');
    $category_definitions = $this->fieldTypeCategoryManager->getDefinitions();
    // Invoke a hook to get category properties.
    foreach ($grouped_definitions as $category => $field_types) {
      foreach ($field_types as $name => $field_type) {
        // Remove fields from the option list.
        if (!empty($field_type_removed[$name])) {
          continue;
        }

        // Add fields to the option list.
        $unique_definitions[$category][$name] = ['unique_identifier' => $name] + $field_type;
        if ($this->fieldTypeCategoryManager->hasDefinition($category)) {
          $category_plugin = $this->fieldTypeCategoryManager->createInstance($category, $unique_definitions[$category][$name], $category_definitions[$category]);
          $field_type_options[$category_plugin->getPluginId()] = ['unique_identifier' => $name] + $field_type;
        }
        else {
          $field_type_options[(string) $field_type['label']] = ['unique_identifier' => $name] + $field_type;
        }
      }
    }

    $field_type_options_radios = [];
    foreach ($field_type_options as $id => $field_type) {
      /** @var  \Drupal\Core\Field\FieldTypeCategoryInterface $category_info */
      $category_info = $this->fieldTypeCategoryManager->createInstance($field_type['category'], $field_type);
      $display_as_group = !($category_info instanceof FallbackFieldTypeCategory);
      $cleaned_class_name = Html::getClass($field_type['unique_identifier']);
      $field_type_options_radios[$id] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['field-option', 'js-click-to-select'],
          'checked' => $this->getRequest()->request->get('new_storage_type') !== NULL && $this->getRequest()->request->get('new_storage_type') == ($display_as_group ? $field_type['category'] : $field_type['unique_identifier']),
        ],
        '#weight' => $category_info->getWeight(),
        'thumb' => [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['field-option__thumb'],
          ],
          'icon' => [
            '#type' => 'container',
            '#attributes' => [
              'class' => [
                'field-option__icon',
                $display_as_group ? "field-icon-$field_type[category]" : "field-icon-$cleaned_class_name",
              ],
            ],
          ],
        ],
        // Store some data we later need.
        '#data' => [
          '#group_display' => $display_as_group,
          '#no_ui' => $field_type['no_ui'],
        ],
        'radio' => [
          '#type' => 'radio',
          '#title' => $category_info->getLabel(),
          '#parents' => ['new_storage_type'],
          '#title_display' => 'before',
          '#description_display' => 'before',
          '#default_value' => $id,
          '#theme_wrappers' => ['form_element__new_storage_type'],
          // If it is a category, set return value as the category label,
          // otherwise, set it as the field type id.
          '#return_value' => $display_as_group ? $field_type['category'] : $field_type['unique_identifier'],
          '#attributes' => [
            'class' => ['field-option-radio'],
          ],
          '#ajax' => [
            'callback' => [$this, 'showFieldsCallback'],
            'event' => 'updateOptions',
            'wrapper' => 'group-field-options-wrapper',
            'progress' => 'none',
            'disable-refocus' => TRUE,
          ],
          '#description' => [
            '#type' => 'container',
            '#attributes' => [
              'class' => ['field-option__description'],
            ],
            '#markup' => $category_info->getDescription(),
          ],
          '#variant' => 'field-option',
        ],
      ];

      if ($libraries = $category_info->getLibraries()) {
        $field_type_options_radios[$id]['#attached']['library'] = $libraries;
      }
    }

    uasort($field_type_options_radios, [SortArray::class, 'sortByWeightProperty']);
    $form['add']['new_storage_type'] = $field_type_options_radios;

    // Set the selected field to the form state by checking
    // the checked attribute.
    $selected_field_type = NULL;
    foreach ($field_type_options_radios as $field_type_options_radio) {
      if ($field_type_options_radio['#attributes']['checked']) {
        $selected_field_type = $field_type_options_radio['radio']['#return_value'];
        $form_state->setValue('selected_field_type', $selected_field_type);
        break;
      }
    }
    if (isset($selected_field_type)) {
      $group_display = $field_type_options_radios[$selected_field_type]['#data']['#group_display'];
      if ($group_display) {
        $form['group_field_options_wrapper']['label'] = [
          '#type' => 'label',
          '#title' => $this->t('Choose an option below'),
          '#required' => TRUE,
        ];
        $form['group_field_options_wrapper']['fields'] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['group-field-options'],
          ],
        ];

        foreach ($unique_definitions[$selected_field_type] as $option_key => $option) {
          $radio_element = [
            '#type' => 'radio',
            '#theme_wrappers' => ['form_element__new_storage_type'],
            '#title' => $option['label'],
            '#description' => [
              '#theme' => 'item_list',
              '#items' => $unique_definitions[$selected_field_type][$option_key]['description'],
            ],
            '#id' => $option['unique_identifier'],
            '#weight' => $option['weight'],
            '#parents' => ['group_field_options_wrapper'],
            '#attributes' => [
              'class' => ['field-option-radio'],
              'data-once' => 'field-click-to-select',
            ],
            '#wrapper_attributes' => [
              'class' => ['js-click-to-select', 'subfield-option'],
            ],
            '#variant' => 'field-suboption',
          ];
          $radio_element['#return_value'] = $option['unique_identifier'];
          if ((string) $option['unique_identifier'] === 'entity_reference') {
            $radio_element['#title'] = 'Other';
            $radio_element['#weight'] = 10;
          }
          $group_field_options[$option['unique_identifier']] = $radio_element;
        }
        uasort($group_field_options, [SortArray::class, 'sortByWeightProperty']);
        $form['group_field_options_wrapper']['fields'] += $group_field_options;
      }

      $no_ui = $field_type_options_radios[$selected_field_type]['#data']['#no_ui'];
      $form['no_ui'] = [
        '#type' => 'value',
        '#value' => $no_ui,
      ];
      if (!$no_ui) {
        $form['group_field_options_wrapper']['display'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Display'),
          '#open' => TRUE,
        ];

        $form['group_field_options_wrapper']['display']['form_display'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Create form display'),
          '#default_value' => TRUE,
        ];

        $form['group_field_options_wrapper']['display']['view_display'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Create view display'),
          '#default_value' => TRUE,
        ];
      }
    }

    $form['new_storage_wrapper']['field_name'] = [
      '#type' => 'machine_name',
      '#size' => 15,
      '#description' => $this->t('A unique machine-readable name containing letters, numbers, and underscores.'),
      // Calculate characters depending on the length of the field prefix.
      // setting. Maximum length is 32.
      '#maxlength' => FieldStorageConfig::NAME_MAX_LENGTH,
      '#machine_name' => [
        'source' => ['new_storage_wrapper', 'label'],
        'exists' => [$this, 'fieldNameExists'],
      ],
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if (!$form_state->getValue('new_storage_type')) {
      $form_state->setErrorByName('new_storage_type', $this->t('You need to select a field type.'));
    }
    elseif (isset($form['group_field_options_wrapper']['fields']) && !$form_state->getValue('group_field_options_wrapper')) {
      $form_state->setErrorByName('group_field_options_wrapper', $this->t('You need to select a field type.'));
    }

    if (!$form_state->getValue('label')) {
      $form_state->setErrorByName('label', $this->t('Add new field: you need to provide a label.'));
    }
    // Missing field name.
    if (!$form_state->getValue('field_name')) {
      $form_state->setErrorByName('field_name', $this->t('Add new field: you need to provide a machine name for the field.'));
    }
    // Field name validation.
    else {
      $field_name = $form_state->getValue('field_name');
      $form_state->setValueForElement($form['new_storage_wrapper']['field_name'], $field_name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $entity_type = $this->entityTypeManager->getDefinition($this->entityTypeId);
    $field_storage_type = $values['group_field_options_wrapper'] ?? $values['new_storage_type'];

    // Create new field.
    if ($field_storage_type) {
      $field_values = [
        'label' => $values['label'],
        'field_name' => $values['field_name'],
        'entity_type' => $this->entityTypeId,
        'field_type' => $field_storage_type,
        'translatable' => $entity_type->isTranslatable(),
      ];

      $widget_id = $formatter_id = NULL;
      $widget_settings = $formatter_settings = [];
      $storage_settings = $field_settings = [];
      // Check if we're dealing with a preconfigured field.
      if (strpos($field_values['field_type'], 'field_ui:') !== FALSE) {
        [, $field_type, $option_key] = explode(':', $field_values['field_type'], 3);
        $field_values['field_type'] = $field_type;

        $field_definition = $this->fieldTypePluginManager->getDefinition($field_type);
        $options = $this->fieldTypePluginManager->getPreconfiguredOptions($field_definition['id']);
        $field_options = $options[$option_key];

        // Merge in preconfigured field storage options.
        if (isset($field_options['field_storage_config'])) {
          $storage_settings = $field_options['field_storage_config']['settings'];
        }

        // Merge in preconfigured field options.
        if (isset($field_options['field_config'])) {
          $field_values['required'] = $field_options['field_config']['required'];
          $field_settings = $field_options['field_config']['settings'];
        }

        $widget_id = $field_options['entity_form_display']['type'] ?? NULL;
        $widget_settings = $field_options['entity_form_display']['settings'] ?? [];
        $formatter_id = $field_options['entity_view_display']['type'] ?? NULL;
        $formatter_settings = $field_options['entity_view_display']['settings'] ?? [];
      }
      else {
        if (!empty($this->fieldTypePluginManager->getDefaultStorageSettings($field_storage_type))) {
          $storage_settings = $this->fieldTypePluginManager->getDefaultStorageSettings($field_storage_type);
        }

        if (!empty($this->fieldTypePluginManager->getDefaultFieldSettings($field_storage_type))) {
          $field_settings = $this->fieldTypePluginManager->getDefaultFieldSettings($field_storage_type);
        }
      }

      if (!empty($values['form_display'])) {
        $field_values['display']['form'] = $values['form_display'];
        $field_values['display']['formatter_type'] = $formatter_id;
        $field_values['display']['formatter_settings'] = $formatter_settings;
      }

      if (!empty($values['view_display'])) {
        $field_values['display']['view'] = $values['view_display'];
        $field_values['display']['widget_type'] = $widget_id;
        $field_values['display']['widget_settings'] = $widget_settings;
      }

      $field_values['field_settings'] = $field_settings;
      $field_values['storage_settings'] = $storage_settings;
      // Create the field storage and field.
      try {
        $field = $this->entityTypeManager->getStorage('easy_entity_field')->create($field_values);
        $field->save();

        if ($values['no_ui']) {
          $form_state->setRedirectUrl($field->toUrl('edit-form'));
        }
        else {
          $form_state->setRedirectUrl($field->toUrl('storage-form'));
        }
      }
      catch (\Exception $e) {
        $this->messenger()->addError($this->t('There was a problem creating field %label: @message', [
          '%label' => $values['label'],
          '@message' => $e->getMessage(),
        ]));
      }
    }

    // Store new field information for any additional submit handlers.
    $form_state->set(['fields_added', '_add_new_field'], $values['field_name']);
  }

  /**
   * Checks if a field machine name is taken.
   *
   * @param string $value
   *   The machine name, not prefixed.
   * @param array $element
   *   An array containing the structure of the 'field_name' element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Whether or not the field machine name is taken.
   */
  public function fieldNameExists($value, $element, FormStateInterface $form_state): bool {
    $field_storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions($this->entityTypeId);
    return isset($field_storage_definitions[$value]);
  }

}
