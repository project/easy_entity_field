<?php

namespace Drupal\easy_entity_field\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * The form for editing EasyEntityField.
 *
 * @package Drupal\easy_entity_field\Form
 */
class EasyEntityFieldForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): ?array {
    $form = parent::form($form, $form_state);

    $form_title = $this->t('%field settings', [
      '%field' => $this->entity->getLabel(),
    ]);
    $form['#title'] = $form_title;

    /**
     * @var \Drupal\easy_entity_field\Entity\EasyEntityFieldInterface $easy_entity_field
     */
    $easy_entity_field = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $easy_entity_field->label(),
      '#weight' => -20,
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Help text'),
      '#default_value' => $easy_entity_field->getDescription(),
      '#rows' => 5,
      '#description' => $this->t('Instructions to present to the user below this field on the editing form.<br />Allowed HTML tags: @tags', ['@tags' => FieldFilteredMarkup::displayAllowedTags()]) . '<br />' . $this->t('This field supports tokens.'),
      '#weight' => -10,
    ];

    $form['required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Required field'),
      '#default_value' => $easy_entity_field->isRequired(),
      '#weight' => -5,
    ];

    // Create an arbitrary entity object (used by the 'default value' widget).
    $ids = (object) [
      'entity_type' => $easy_entity_field->getTargetEntityTypeId(),
      'bundle' => $easy_entity_field->getTargetEntityTypeId(),
      'entity_id' => NULL,
    ];

    $form['#entity'] = _field_create_entity_from_ids($ids);

    if (!$items = $form['#entity']->get($easy_entity_field->getName())) {
      return NULL;
    }
    $item = $items->first() ?: $items->appendItem();

    /**
     * @var \Drupal\easy_entity_field\Plugin\EasyEntityFieldBase $field_plugin
     */
    $field_plugin = $easy_entity_field->hasFieldPlugin() ? $easy_entity_field->getFieldPlugin() : '';

    $field_settings_element = '';
    if (!empty($field_plugin) && !empty($field_plugin->fieldSettingsForm($form, $form_state))) {
      $field_settings_element = $field_plugin->fieldSettingsForm($form, $form_state);
    }
    elseif (!empty($item->fieldSettingsForm($form, $form_state))) {
      $field_settings_element = $item->fieldSettingsForm($form, $form_state);
    }

    if ($field_settings_element) {
      $field_settings_element = array_merge($field_settings_element, [
        '#type' => 'details',
        '#title' => $this->t('settings'),
        '#open' => TRUE,
        '#tree' => TRUE,
        '#description' => $this->t('Enable to add a path field and allow to define alias patterns for the given type. Disabled types already define a path field themselves or currently have a pattern.'),
        '#weight' => 12,
      ]);

      $form['settings'] = $field_settings_element;
    }

    if ($default_values_element = $items->defaultValuesForm($form, $form_state)) {
      $has_required = $this->hasAnyRequired($default_values_element);
      $default_values_element = array_merge($default_values_element, [
        '#type' => 'details',
        '#title' => $this->t('Default value'),
        '#open' => TRUE,
        '#tree' => TRUE,
        '#description' => $this->t('The default value for this field, used when creating new content.'),
        '#weight' => 20,
      ]);

      if (!$has_required) {
        $has_default_value = count($this->entity->getDefaultValue($form['#entity'])) > 0;
        $default_values_element['#states'] = [
          'invisible' => [
            ':input[name="set_default_value"]' => ['checked' => FALSE],
          ],
        ];
        $form['set_default_value'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Set default value'),
          '#default_value' => $has_default_value,
          '#description' => $this->t('Provide a pre-filled value for the editing form.'),
          '#weight' => $element['#weight'] ?? 15,
        ];
      }
      $form['default_value'] = $default_values_element;
    }

    $form['#prefix'] = '<div id="field-combined">';
    $form['#suffix'] = '</div>';
    $form['#attached']['library'][] = 'field_ui/drupal.field_ui';

    return $form;
  }

  /**
   * A function to check if element contains any required elements.
   *
   * @param array $element
   *   An element to check.
   *
   * @return bool
   *   TRUE if element contains any required elements, FALSE otherwise.
   */
  private function hasAnyRequired(array $element) {
    $has_required = FALSE;
    foreach (Element::children($element) as $child) {
      if (isset($element[$child]['#required']) && $element[$child]['#required']) {
        $has_required = TRUE;
        break;
      }
      if (Element::children($element[$child])) {
        return $this->hasAnyRequired($element[$child]);
      }
    }

    return $has_required;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save settings');

    if (!$this->entity->isNew()) {
      $actions['delete'] = [
        '#type' => 'link',
        '#title' => $this->t('Delete'),
        '#url' => $this->entity->toUrl('delete-form'),
        '#access' => $this->entity->access('delete'),
        '#attributes' => [
          'class' => ['button', 'button--danger'],
        ],
      ];
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
    if (isset($form['default_value'])) {
      $item = $form['#entity']->get($this->entity->getName());
      $item->defaultValuesFormValidate($form['default_value'], $form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    if (isset($form['settings']) && $form_state->getValue('settings')) {
      $this->entity->setFieldSettings($form_state->getValue('settings'));
    }

    // Handle the default value.
    $default_value = [];
    if (isset($form['default_value']) && (!isset($form['set_default_value']) || $form_state->getValue('set_default_value'))) {
      $items = $form['#entity']->get($this->entity->getName());
      $default_value = $items->defaultValuesFormSubmit($form['default_value'], $form, $form_state);
    }

    $this->entity->setDefaultValue($default_value);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $easy_entity_field = $this->entity;
    $easy_entity_field->save();
    $this->messenger()->addStatus($this->t('Saved %label configuration.', ['%label' => $this->entity->getLabel()]));
    $form_state->setRedirectUrl($easy_entity_field->toUrl('collection'));
  }

}
