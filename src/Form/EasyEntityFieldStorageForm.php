<?php

namespace Drupal\easy_entity_field\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class EasyEntityFieldForm.
 */
class EasyEntityFieldStorageForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): ?array {
    $form = parent::form($form, $form_state);

    /**
     * @var \Drupal\easy_entity_field\Entity\EasyEntityFieldInterface $easy_entity_field ;
     */
    $easy_entity_field = $this->entity;
    $form['#title'] = $easy_entity_field->getLabel();

    // Create an arbitrary entity object, so that we can have an instantiated
    // FieldItem.
    $ids = (object) [
      'entity_type' => $easy_entity_field->getTargetEntityTypeId(),
      'bundle' => $easy_entity_field->getTargetEntityTypeId(),
      'entity_id' => NULL,
    ];
    $entity = _field_create_entity_from_ids($ids);

    if (!$items = $entity->get($easy_entity_field->getName())) {
      return NULL;
    }

    $item = $items->first() ?: $items->appendItem();

    /**
     * @var \Drupal\easy_entity_field\Plugin\EasyEntityFieldBase $field_plugin
     */
    $field_plugin = $easy_entity_field->hasFieldPlugin() ? $easy_entity_field->getFieldPlugin() : '';

    $storage_settings_element = '';
    if (!empty($field_plugin) && !empty($field_plugin->storageSettingsForm($form, $form_state, $easy_entity_field->hasData()))) {
      $storage_settings_element = $field_plugin->storageSettingsForm($form, $form_state, $easy_entity_field->hasData());
    }
    elseif (!empty($item->storageSettingsForm($form, $form_state, $easy_entity_field->hasData()))) {
      $storage_settings_element = $item->storageSettingsForm($form, $form_state, $easy_entity_field->hasData());
    }

    if (!empty($storage_settings_element)) {
      $storage_settings_element = array_merge($storage_settings_element, [
        '#type' => 'details',
        '#weight' => -10,
        '#open' => TRUE,
        '#tree' => TRUE,
        '#title' => $this->t('Settings storage'),
      ]);

      $form['settings'] = $storage_settings_element;
      if ($easy_entity_field->hasData()) {
        $form['settings']['#description'] = '<div class="messages messages--error">' . $this->t('There is data for this field in the database. The field settings can no longer be changed.') . '</div>';
      }
    }
    else {
      $form['no_setting'] = [
        '#type' => 'item',
        '#title' => $this->t('No setting'),
        '#markup' => $this->t('No setting is required, please go to the next step'),
      ];
      $form_state->setValue('storage_settings', FALSE);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);

    if (!empty($form_state->getValue('storage_settings'))) {
      $actions['submit']['#value'] = $this->t('Save field settings');
    }
    else {
      $actions['submit']['#value'] = $this->t('Next Steps');
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    if (isset($form['settings']) && $form_state->getValue('settings')) {
      $this->entity->setStorageSettings($form_state->getValue('settings'));
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $easy_entity_field = $this->entity;
    parent::save($form, $form_state);

    $form_state->setRedirectUrl($easy_entity_field->toUrl('edit-form'));
  }

}
